import tkinter as tk
from threading import Event, Thread
import time
import os
import vlc

"""timer thread to update the playing time information"""
class timer(Thread):
    def __init__(self,interval,callbackFunction):
        Thread.__init__(self)
        self.interval=interval
        self.callbackFunction=callbackFunction
        self.event=Event()

    def run(self):
        while not self.event.wait(self.interval):
            self.callbackFunction()

    def stop(self):
        self.event.set()
        

class ssma_app(tk.Tk):

    attributeFile="attribute.txt"
    playlistFile="playlist.txt"
    outputFile="output.txt"

    attributesLabelDict=[]
    playlistValueDict=[]
    attributeValuesDict=[]

    playListIndex=0

    isValuesSaved=False
    
    def __init__(self,parent):
        tk.Tk.__init__(self,parent)
        self.parent=parent

        self.protocol("WM_DELETE_WINDOW", self.OnExit)
        
        self.redAttribute()
        
        #initialize output file
        rowStr="Item"
        for attribute in self.attributesLabelDict:
            rowStr=rowStr+"\t"+attribute['attribute']
        self.writeIntoOutputFile(rowStr)
                
        self.redPlaylist()
        self.createWidgets()

        # VLC player controls
        self.Instance = vlc.Instance()
        self.player = self.Instance.media_player_new()
        
        self.playerTimer = timer(1,self.updateTime)
        self.playerTimer.start()

    def createWidgets(self):
        self.grid()

        self.timerValue=tk.StringVar()
        self.trackTime=tk.StringVar()
        self.trackName=tk.StringVar()        
        
        self.label=tk.Label(self,text="Rate, how much the attributes apply to the audio signal:",anchor='e')
        self.label.grid(column=0,row=0)

        attributePane=tk.PanedWindow(self.parent)
        self.createAttributePanel(attributePane)
        attributePane.grid(column=0,row=1)

        infoPane=tk.PanedWindow(self.parent)
        self.createInfoPanel(infoPane)
        infoPane.grid(column=0,row=2,sticky='w')
        
        controlPane=tk.PanedWindow(self.parent)
        self.createControlPanel(controlPane)
        controlPane.grid(column=0,row=3)
        
    # create a panel to show all the available attributes and slider to select values fro given attribute
    def createAttributePanel(self,attributePane):
        rowNum=0
        for attribute in self.attributesLabelDict:
            label=tk.Label(attributePane,text=attribute['attribute'])
            label.grid(column=0,row=rowNum,sticky="w")

            minLabel=tk.Label(attributePane,text=attribute['minLabel'])
            minLabel.grid(column=1,row=rowNum,sticky="e")

            slider=tk.Scale(attributePane,from_=0,to=10,cursor="arrow",orient="horizontal",length="200")
            slider.grid(column=2,row=rowNum)

            maxLabel=tk.Label(attributePane,text=attribute['maxLabel'])
            maxLabel.grid(column=3,row=rowNum,sticky="w")

            self.attributeValuesDict.append({'attribute': attribute['attribute'], 'slider': slider})

            rowNum=rowNum+1

    # create a panel to display audio track being played and progress
    def createInfoPanel(self,infoPane):
        playingLabel=tk.Label(infoPane,textvariable=self.trackName)
        playingLabel.grid(column=0,row=0,sticky="w")

        emptyLabel=tk.Label(infoPane,text=" ",padx=20)
        emptyLabel.grid(column=1,row=0)
        
        timeLabel=tk.Label(infoPane,textvariable=self.timerValue)
        timeLabel.grid(column=2,row=0)

        barLabel=tk.Label(infoPane,text="/")
        barLabel.grid(column=3,row=0)

        trackTimeLabel=tk.Label(infoPane,textvariable=self.trackTime)
        trackTimeLabel.grid(column=4,row=0)

        self.timerValue.set("00:00")

    # create a panel to present user buttons to play audio and save values
    def createControlPanel(self,controlPane):
        button = tk.Button(controlPane,text=u"Play",command=self.OnPlay)
        button.grid(column=0,row=0)

        button = tk.Button(controlPane,text=u"Pause",command=self.OnPause)
        button.grid(column=1,row=0)

        button = tk.Button(controlPane,text=u"Stop",command=self.OnStop)
        button.grid(column=2,row=0)
        
        button = tk.Button(controlPane,text=u"Next",command=self.OnNext)
        button.grid(column=3,row=0)

        emptyLabel=tk.Label(controlPane,text=" ",padx=30)
        emptyLabel.grid(column=4,row=0)
        
        button = tk.Button(controlPane,text=u"Save value",command=self.OnSave)
        button.grid(column=5,row=0)
        
    def OnPlay(self):
        if not self.player.get_media():
            media = self.Instance.media_new('playlist\\'+self.playlistValueDict[self.playListIndex]['item'])
            self.player.set_media(media)
            self.player.play()
            self.trackName.set(self.playlistValueDict[self.playListIndex]['item'])

            self.updateTrackLength()
        else:
            self.player.play()

    def OnPause(self):
        self.player.pause()

    def OnStop(self):
        self.player.stop()
        self.timerValue.set("00:00")

    def OnNext(self):
        self.player.stop()

        self.playListIndex=self.playListIndex+1
        if self.playListIndex>=self.playlistValueDict.__len__():
            self.playListIndex=0

        media = self.Instance.media_new('playlist\\'+self.playlistValueDict[self.playListIndex]['item'])
        self.player.set_media(media)
        self.player.play()
        self.trackName.set(self.playlistValueDict[self.playListIndex]['item'])
        self.updateTrackLength()

        #reset this flag in order to save attribute values for new track
        self.isValuesSaved=False
        
        self.resetSliderValue()
        
    def resetSliderValue(self):
        for attributeValue in self.attributeValuesDict:
            attributeValue['slider'].set(0)

    def OnSave(self):
        if not self.isValuesSaved:
            rowStr=self.playlistValueDict[self.playListIndex]['item']
            for attributeValue in self.attributeValuesDict:
                rowStr=rowStr+"\t"+str(attributeValue['slider'].get())
            self.writeIntoOutputFile(rowStr)
            self.isValuesSaved=True
                
    def updateTime(self):
        if self.player == None:
            return
        if not self.player.get_media():
            return
        
        timeStr = self.getTimeFormatted(self.player.get_time())
        self.timerValue.set(timeStr)

    def updateTrackLength(self):
        time.sleep(0.5)
        timeStr = self.getTimeFormatted(self.player.get_length())
        self.trackTime.set(timeStr)

    def getTimeFormatted(self,timeInMilliSec):
        seconds = timeInMilliSec / 1000;
        minutes = seconds / 60;
        seconds = seconds%60
        minutes = minutes%60
        timeStr = str(int(minutes))+":"+str(int(seconds))
        return timeStr
        
    #read all the attributes from attribute.txt file
    def redAttribute(self):
        with open(self.attributeFile) as f:
            for line in f:
               attributes = line.split(';')
               self.attributesLabelDict.append({'attribute': attributes[0], 'minLabel': attributes[1], 'maxLabel': attributes[2]})

    #read all the playlists from playlist.txt file
    def redPlaylist(self):
        with open(self.playlistFile) as f:
            for line in f:
               playlist = line.split(';')
               self.playlistValueDict.append({'item': playlist[0], 'deviceId': playlist[1]})

    #write values to file
    def writeIntoOutputFile(self,row):
        with open(self.outputFile,mode='a') as f:
            f.write(row+"\n")

    def OnExit(self):
        self.player.stop()
        self.playerTimer.stop()
        self.destroy()
        os._exit(1)
        
if __name__ == "__main__":
    app=ssma_app(None)
    app.title('Multi dimensional audio quality evaluation')
    app.mainloop()
